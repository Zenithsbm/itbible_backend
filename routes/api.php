   <?php

use Illuminate\Http\Request;

//header('Access-Control-Allow-Origin: *');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



// Route::post('register', 'API\PassportController@register');
Route::get('test', 'EncryTestController@encrypttest');
Route::get('testd', 'EncryTestController@decrypttest');
Route::post('login', 'API\PassportController@login');
Route::get('auth/{provider}', 'API\PassportController@redirectToProvider');
Route::get('auth/{provider}/callback', 'API\PassportController@handleProviderCallback');


//-------------------------User Group -----------------------------------------

//Here we set a usergroup  for API authentication
Route::group(['middleware' => 'auth:api'],function(){
  //----------List all Sub Clients in Clients_controller----------------

  //Get sub clients give  a post requests
  Route::get('subclients','Sub_client@index');
  //to get the details of the loged user
  Route::get('get_user', 'API\PassportController@getDetails');
  //List single Sub clients in Clients_controller
  Route::get('subclient/{id}','Sub_client@show');
  //Create New Sub clients in Clients_controller
  Route::post('subclient','Sub_client@store');
  //Update Sub Clients in Clients_controller
  Route::put('subclient','Sub_client@update');
   Route::put('subclient_all','Sub_client@update_all');
  
  //Delete Sub Clients using sub_id and c_id
  Route::delete('subclient/{id}','Sub_client@destroy');
  //full subclients detail for user
  Route::post('clients','Sub_client@clients');
  //Get details by a specific user id
  Route::get('clients_sub/{id}','Sub_client@clients_sub');

  //----------List all Sites in Clients_controller----------------
  Route::get('sites','SiteController@index');
  //List single type in controller
  Route::get('site/{id}','SiteController@show');

  //List single site in controller
  Route::get('site_id/{id}','SiteController@show_id');
  //Create New type in controller
  Route::post('site','SiteController@store');
  //Update type in controller
  Route::put('site/{id}','SiteController@update');
  //Delete type
  Route::delete('site/{id}','SiteController@destroy');


  //----------List all Types in Clients_controller----------------
  Route::get('types','TypeController@index');
  //List single type in controller
  Route::get('type/{id}','TypeController@show');
  //Create New type in controller
  Route::post('type','TypeController@store');
  //Update type in controller
  Route::put('type','TypeController@update');
  //Delete type
  Route::delete('type/{id}','TypeController@destroy');




  //----------List all Assets in controller----------------
  Route::get('assets','AssetsController@index');
  //List single Asstes-detail in controller
  Route::get('asset/{id}','AssetsController@show');
    //List single Asstes-detail in controller by user_id
  Route::get('assetby_user/{id}','AssetsController@showby_user');
  //Take particular item according to Asstes id
  Route::get('assetby_type/{id}','AssetsController@showby_type');
  Route::post('asset','AssetsController@store');
  //Update Asstes-detail in Controller
  Route::put('asset','AssetsController@update');
  //delete by Asstes id
  Route::delete('asset/{id}','AssetsController@destroy');

  //----------List all KeypairHeadin Details in KeypairHeadingController-------

  Route::post('add_keypairheadings', 'KeypairHeadingController@store');
  Route::put('update_keypairheadings', 'KeypairHeadingController@update');
  Route::post('keypairheadings', 'KeypairHeadingController@show');
  Route::put('delete', 'KeypairHeadingController@destroy');


  //----------List all Keypair Details in Clients_controller----------------
  Route::get('keypairs','KeypairsController@index');
  //List single  Keypair contents controller
  Route::post('keypairbyid','KeypairsController@show');
  //Create New Keypair content controller
  Route::post('keypair','KeypairsController@store');
  //Update Keypair content in controller
  Route::put('keypair','KeypairsController@update');
  
  //delete Keypair content in controller
  Route::delete('keypair/{id}','KeypairsController@destroy');
  
  //if Decrypt and encrypt keypair content
  Route::put('keypair_inscribe','KeypairsControllerED@update');
  Route::post('keypair_inscribe','KeypairsControllerED@store');
  Route::get('keypair_render/{key_value}','KeypairsController@decrypt');



  //----------List all Accesstable in controller----------------
  Route::get('accesslist','AccesstableController@index');
  //to get distict group id
  Route::post('groupid','AccesstableController@distinctGroupid');
  //List single Asstes-detail in controller
  Route::post('access_permission','AccesstableController@show');
  //Take particular item according to Asstes id
  Route::post('access','AccesstableController@store');
  //Update Asstes-detail in Controller
  Route::put('access','AccesstableController@update');
  //delete by Asstes id
  Route::delete('access/{id}','AccesstableController@destroy');
  //Decrypt
  Route::post('access_render','AccesstableController@decrypt');


//LogOut
Route::post('logout','LogoutController@logout');
});

//---------------------------------- Admin Group -------------------------------

Route::group(['middleware' => 'IsAdmin:api'], function () {

  //Get sub clients give  a post requests
  Route::get('subclients-admin','AdminControllers\SubclientAdminController@index');
  //List single Sub clients in Clients_controller
  Route::get('subclient-admin/{id}','AdminControllers\SubclientAdminController@show');
  //Create New Sub clients in Clients_controller
  Route::post('subclient-admin','SAdminControllers\SubclientAdminController@store');
  //Update Sub Clients in Clients_controller
  Route::put('subclient-admin','AdminControllers\SubclientAdminController@update');
  //Delete Sub Clients using sub_id and c_id
  Route::delete('subclient-admin','AdminControllers\SubclientAdminController@destroy');
  //full subclients detail for user
  Route::get('clients-admin','AdminControllers\SubclientAdminController@clients');


  //----------List all Types in Clients_controller--------------
  Route::get('types-admin','AdminControllers\TypeAdminController@index');
  //List single type in controller
  Route::get('type-admin/{id}','AdminControllers\TypeAdminController@show');
  //Create New type in controller
  Route::post('type-admin','AdminControllers\TypeAdminController@store');
  //Update type in controller
  Route::put('type-admin','AdminControllers\TypeAdminController@update');
  //Delete type
  Route::delete('type-admin','AdminControllers\TypeAdminController@destroy');
  Route::put('type_flag-admin','AdminControllers\TypeAdminController@update_delete_flag');

  //----------List all Assets in controller----------------
  Route::get('assets-admin','AdminControllers\AssetsAdminController@index');
  //List single Asstes-detail in controller
  Route::get('asset-admin/{id}','AdminControllers\AssetsAdminController@show');
  //Take particular item according to Asstes id
  Route::get('assetby_type-admin/{id}','AdminControllers\AssetsAdminController@showby_type');
  Route::post('asset-admin','AdminControllers\AssetsAdminController@store');
  //Update Asstes-detail in Controller
  Route::put('asset-admin','AdminControllers\AssetsAdminController@update');
  //delete by Asstes id
  Route::delete('asset-admin','AdminControllers\AssetsAdminController@destroy');
  Route::put('asset_flag-admin','AdminControllers\AssetsAdminController@update_delete_flag');


  //----------List all Keypair Details in Clients_controller------
  Route::get('keypairs-admin','AdminControllers\KeypairsAdminController@index');
  //List single  Keypair contents controller
  Route::get('keypair-admin/{id}','AdminControllers\KeypairsAdminController@show');
  Route::get('keypairby_user-admin','AdminControllers\KeypairsAdminController@showpairby_user');
  //Create New Keypair content controller
  Route::post('keypair-admin','AdminControllers\KeypairsAdminController@store');
  //Update Keypair content in controller
  Route::put('keypair-admin','AdminControllers\KeypairsAdminController@update');
  //Delete Template content
  Route::delete('keypair-admin','AdminControllers\KeypairsAdminController@destroy');
  Route::put('keypair_flag-admin','AdminControllers\KeypairsAdminController@update_delete_flag');


  //----------List all Accesstable in controller----------------
  Route::get('accesslist-admin','AdminControllers\AccesstableAdminController@index');
  //List single Asstes-detail in controller
  // Route::get('access-admin/{id}','AdminControllers\AccesstableAdminController@show');
  //Take particular item according to Asstes id
  Route::get('accessby_type-admin/{id}','AdminControllers\AccesstableAdminController@showby_type');
  Route::post('access-admin','AdminControllers\AccesstableAdminController@store');
  //Update Asstes-detail in Controller
  Route::put('access-admin','AdminControllers\AccesstableAdminController@update');
  //delete by Asstes id
  Route::delete('access-admin','AdminControllers\AccesstableAdminController@destroy');
  Route::put('access_flag-admin','AdminControllers\AccesstableAdminController@update_delete_flag');

  //LogOut
 // Route::post('logout','LogoutController@logout');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
