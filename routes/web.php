<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
Route::post('begin', 'DeploySettingsController@install');
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'API\PassportController@redirectToProvider');
Route::get('auth/{provider}/callback', 'API\PassportController@handleProviderCallback');






// Route::get('doc/callback', function (Request $request) {
//     $http = new GuzzleHttp\Client;
//
//     $response = $http->post('http://localhost:8000/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'authorization_code',
//             'client_id' => '9',
//             'client_secret' => 'Twkk33TQ93brgBN9elfnEDmkoaHOg4HmlfVeLKKd',
//             'redirect_uri' => 'http://localhost:8001/doc/callback',
//             'code' => $request->code,
//         ],
//     ]);
//
//     return json_decode((string) $response->getBody(), true);
// });
