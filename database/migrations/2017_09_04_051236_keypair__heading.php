<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeypairHeading extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('keypair_headings', function (Blueprint $table) {
           $table->increments('id');
           $table->string('headings');
           $table->integer('asset_id')->unsigned();
           $table->foreign('asset_id')->references('id')->on('assets');
           $table->integer('parent_id');
           $table->integer('user_id')->unsigned();
           $table->foreign('user_id')->references('id')->on('users');
           $table->timestamps();
           $table->softDeletes();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
           Schema::drop('keypair_heading');
     }
}
