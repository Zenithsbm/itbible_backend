<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_tables', function (Blueprint $table) {
          $table->increments('id');
          //$table->unsignedInteger('keypair_id')->nullable();
          //$table->foreign('keypair_id')->references('id')->on('keypairs')->onDelete('cascade');
          $table->integer('keypair_id')->unsigned();
          $table->foreign('keypair_id')->references('id')->on('keypairs');
        //   $table->integer('group_id')->unsigned();
        //   $table->foreign('group_id')->references('id')->on('keypair_headings');
          $table->integer('permission_user_id')->unsigned();
          $table->foreign('permission_user_id')->references('id')->on('users');
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
          $table->string('access_permission');
          $table->integer('site_id')->unsigned();
          $table->foreign('site_id')->references('id')->on('sites');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_tables');
    }
}
