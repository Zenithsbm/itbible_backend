<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(

            [
                'name' => 'Ellaware',
                'email' => 'admin@ellaware.info',
                'password' => bcrypt('admin'),
                'type'=>'admin',
                'status'=>'1',
                'contact'=>'1212454512',
                'parent_id'=>'0',
                'location'=>'Australia',
                'address'=>'address',
                'user_id'=>'0'
            ]
        );
    }
}
