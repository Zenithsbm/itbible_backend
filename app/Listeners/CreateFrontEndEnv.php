<?php

namespace App\Listeners;
use RachidLaasri\LaravelInstaller\Events\LaravelInstallerFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateFrontEndEnv
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LaravelInstallerFinished  $event
     * @return void
     */
    public function handle(LaravelInstallerFinished $event)
    {
        //please check this code in deploycontroller
    }
}
