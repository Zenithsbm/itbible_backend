<?php

namespace App\Listeners;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RachidLaasri\LaravelInstaller\Events\LaravelInstallerFinished;
use Illuminate\Support\Facades\Artisan;


class ArtisanCommands
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LaravelInstallerFinished  $event
     * @return void
     */
    public function handle(LaravelInstallerFinished $event)
    {
        Artisan::call('key:generate');
        Artisan::call('passport:install');
        // Artisan::call('passport:key');
       
    }
}
