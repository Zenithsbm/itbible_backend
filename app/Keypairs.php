<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

class Keypairs extends Model implements AuditableContract
{
  use Auditable;
  use \OwenIt\Auditing\Auditable;
    //

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [

        'asset_id','user_id','keypair_name','keypair_value'
    ];

}
