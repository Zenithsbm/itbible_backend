<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

class Access_tables extends Model implements AuditableContract
{
  use Auditable;
  use \OwenIt\Auditing\Auditable;
    //

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
