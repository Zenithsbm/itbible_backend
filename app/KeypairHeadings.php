<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class KeypairHeadings extends Model implements AuditableContract
{
  use Auditable;
  use \OwenIt\Auditing\Auditable;
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'headings','asset_id','parent_id','user_id'
    ];

}
