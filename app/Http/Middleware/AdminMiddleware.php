<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


    if (Auth::guard('api')->user() && Auth::guard('api')->user()->type ==='admin') {
      return $next($request);
    }
      return redirect(404);

    }
}
