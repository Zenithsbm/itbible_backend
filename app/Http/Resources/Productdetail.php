<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Productdetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

      //  echo $request[0]['product_value'];
    }
}
