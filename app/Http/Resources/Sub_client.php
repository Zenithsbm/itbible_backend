<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Sub_client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);


        return[
          'id'=>$this->id,
          'c_id'=>$this->name,
          'username'=>$this->username,
          // 'location'=>$this->location,
          // 'address'=>$this->address,
          // 'email'=>$this->email,

        ];
    }
}
