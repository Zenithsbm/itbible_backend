<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class LogoutController extends Controller
{
  public function logout() {
 //$user = Auth::user();
 //$accessdata->user_id = $user->id;
 // echo $accessdata; exit;
 
 
 
    
      $accessToken = Auth::user()->token();
     // echo "ok";exit;
     
      
      DB::table('oauth_refresh_tokens')
          ->where('access_token_id', $accessToken->id)
          ->update([
              'revoked' => true
          ]);

      $accessToken->revoke();
      return response()->json(['message' => 'Successfully logged out'],200);
  }
}
