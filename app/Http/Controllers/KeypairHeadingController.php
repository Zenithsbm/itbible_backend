<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\KeypairHeadings;
use App\Asset;
use App\Type;
use App\User;
use App\Keypairs ;
use App\Access_tables ;
use App\Http\Resources\Keypair as KeypairResource;
use DB;


class KeypairHeadingController extends Controller
{

    public $sucessStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
   {
     $heading_new = array();
     $array = $request->all();
     $i = 0;
     foreach($array["data"] as $row)
     {
         $new_heading = array();
         $old_heading = array();
         $heading = array();
         $parent_id=0;
     $array = $request->all();
             $i = 0;
   foreach($array["data"] as $row)
      {
        //checking new heading or old one

                        if($row["new_heading"]===null){
                        $parent_id =$row["old_heading"];
                        $kp = DB::table('keypair_headings')
                        ->select('id')
                        ->where('parent_id','=',$parent_id)
                        ->orderBy('id', 'DESC')
                        ->limit(1)
                        ->get();

                        foreach($kp as $kps){
                              $k_id=$kps->id;
                             // echo $parent_id;
                       }

                         }elseif ($row["old_heading"]===null) {

                          $new_heading = $row["new_heading"];

                          if($i>0){
                            //fetching last inserted data
                                        $parents = DB::table('keypair_headings')
                                        ->select('id')
                                        ->where('user_id','=',$row["user_id"])
                                        ->orderBy('id', 'DESC')
                                        ->limit(1)
                                        ->get();

                                        foreach($parents as $parent){
                                              $parent_id=$parent->id;
                                             // echo $parent_id;
                            }

                          }
                          //insertion of keypair data
                                                  $keypair =   KeypairHeadings::create([
                                                 'headings'   => $new_heading,
                                                 'asset_id'   => $row["asset_id"],
                                                 'parent_id'  => $parent_id,
                                                 'user_id'  => $row["user_id"],

                             ]);
                               $i++;

                               $kp = DB::table('keypair_headings')
                               ->select('id')
                               ->where('parent_id','=',$parent_id)
                               ->orderBy('id', 'DESC')
                               ->limit(1)
                               ->get();

                               foreach($kp as $kps){
                                     $k_id=$kps->id;
                                    // echo $parent_id;
                              }

                        } else {

                        }
        }
 //correct value of i
 //echo $i;
 //echo $parent_id;

 ///display inserted item

 // print_r($keypair);

         // if($keypair)
         // {
           // foreach ($keypair as $key ) {
           //
           //             // Access $client->sku here...
           //
           // $x[]=  [
           //     'headings' => $key->headings,
           //     // 'old_heading' => $key->old_heading,
           //     'asset_id' => $key->asset_id,
           //     'parent_id' => $key->parent_id,
           //     'user_id' => $key->user_id,
           //     'id'=>$key->id,
           //
           //     ];
           //
           //
           // }

           return response()->json(['success'=>$k_id],$this->sucessStatus);
           // }

     }

   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $id=null;
      $parent_id=null;
      $asset_id=null;
      $key_heading= "no data";

          $id             = $request->input('id');
          $parent_id      = $request->input('parent_id');
          $asset_id       = $request->input('asset_id');

//fetch according to id
if($parent_id==null && $asset_id ==null){
$key_heading  = KeypairHeadings::findorfail($id);
  //  $key_heading    = KeypairHeadings::where('id','=',$id)->where('asset_id','=',$asset_id)->get();
}
          if($parent_id==null && $asset_id !=null && $id !=null){
           //$key_heading  = KeypairHeadings::findorfail($id);
              $key_heading    = KeypairHeadings::where('id','=',$id)->where('asset_id','=',$asset_id)->get();
          }

//fetch according to parent id

          if($id==null){

          $key_heading    = KeypairHeadings::where('parent_id','=',$parent_id)->where('asset_id','=',$asset_id)->get();
          }

//fetch according to asset id

          if($id==null && $parent_id==null){

          $key_heading    = KeypairHeadings::where('asset_id','=',$asset_id)->get();
          }

          //fetch according to asset id

                    if($parent_id==null && $id !=null && $asset_id !=null){

                    $key_heading    = KeypairHeadings::where('asset_id','=',$asset_id)->where('id','=',$id)->get();
                    }
                    
                    
                            //fetch according to all conditions
                    if($parent_id !=null && $asset_id !=null && $id !=null){
                $key_heading    = KeypairHeadings::where('asset_id','=',$asset_id)->where('id','=',$id)->where('parent_id ','=',$parent_id)->get();
                     
                    }


          return response()->json(['success'=>$key_heading],$this->sucessStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //this is up
    public function destroy(Request $request)
    {

      $data_id        = array();
      $existing_data  = array();
      $updated_data   = array();
      $new_heading    = array();

   $last_id  = $request->input('last_id');
   $asset_id = $request->input('asset_id');
   $user_id  =  $request->input('user_id');

   $array = $request->all();

     $i = 0;
     foreach($array["data"] as $row)
        {
          //checking new heading or old one
          //if updations only

                               if($row["data_id"]!=null&&$row["updated_data"]!=null)
                               {
                                 $id              = $row["data_id"];
                                 $keypair_heading = $row["updated_data"];
                                 $key = KeypairHeadings::where('id', $id)->update(array('headings' =>$keypair_heading));
                                }
//if new insertion coming
if($row["new_heading"]!=null){

   $id              = $row["data_id"];
   $new_heading     = $row["new_heading"];

      $key = KeypairHeadings::create([
        'headings' =>$new_heading,
        'asset_id'=>$asset_id,
        'parent_id'=>$id,
        'user_id'=>$user_id
      ]);
$lastinsert_id =  $key->id;

//to update the keypair id table if insertion in last node

if($id==$last_id){
$key = Keypairs::where('heading_id', $last_id)->update(array('heading_id' =>$lastinsert_id));
}
//to update the next child parent id incase of a new insertion
else{
$key = KeypairHeadings::where('parent_id','=', $id)->where('id', '!=', $lastinsert_id)->update(array('parent_id' =>$lastinsert_id));
}

}
        }
  return response()->json(['success'=>"data updated"],$this->sucessStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $data_id        = array();
        $existing_data  = array();
        $updated_data   = array();
        $new_heading    = array();
        $delete_id      = array();
        $x="";
        $k=0;

      $last_id  = $request->input('last_id');
      $asset_id = $request->input('asset_id');
      $user_id  =  $request->input('user_id');

      $array = $request->all();

       $i = 0;
       foreach($array["data"] as $row)
          {
            //deletion
            //checking new heading or old one
            if($row["data_id"]!=null&&$row["delete_id"]!=null)
            {
              $id              = $row["data_id"];
              $delete          = $row["delete_id"];

              //end node deletion
              if($delete==$last_id)
              {
                $keypair = KeypairHeadings::where('id', $delete)->firstOrFail();
                $par_id=$keypair->parent_id;

                //if parent id is obtained
                if($par_id)
                {
                  $access  = Access_tables::where('group_id', $delete)->update(array('group_id' =>$par_id));
                  $keypair = KeypairHeadings::where('id', $id)->firstOrFail();

    //if deletion performed updation will perform

                  if($access){
                    $key = Keypairs::where('heading_id', $delete)->update(array('heading_id' =>$par_id));
                    $keypair->delete();
                  }



                }
              }
              else{

              $keypair = KeypairHeadings::where('id', $id)->firstOrFail();
              $par_id=$keypair->parent_id;

              $key = KeypairHeadings::where('parent_id', $delete)->update(array('parent_id' =>$par_id));
              // if($key){
              $keypair = KeypairHeadings::where('id', $id)->firstOrFail();
              $keypair->delete();
            // }
          }
            }
//end deletion

            //if updations only

                                 if($row["data_id"]!=null&&$row["updated_data"]!=null)
                                 {
                                   $id              = $row["data_id"];
                                   $keypair_heading = $row["updated_data"];
                                   $key = KeypairHeadings::where('id', $id)->update(array('headings' =>$keypair_heading));
                                 }
      //if new insertion coming
      if($row["new_heading"]!=null){

      $id              = $row["data_id"];
      $new_heading     = $row["new_heading"];
if($row["data_id"]!=null){
        $key = KeypairHeadings::create([
          'headings' =>$new_heading,
          'asset_id'=>$asset_id,
          'parent_id'=>$id,
          'user_id'=>$user_id
        ]);
          $lastinsert_id =  $key->id;
      }


      //if last id is null, ie insertion of multiple items one below other
//   if($row["data_id"]==""){
//
//     $keyz= KeypairHeadings::create([
//       'headings' =>$new_heading,
//       'asset_id'=>$asset_id,
//       'parent_id'=>$lastinsert_id,
//       'user_id'=>$user_id
//     ]);
//   //  print_r($key);
//      $lastinsert_id2 =  $keyz->id;
//       $x =$lastinsert_id;
// $k=1;
//   }

      //to update the keypair id table if insertion in last node
//   if($k=1){
//
// echo $x;
//      $key = Keypairs::where('heading_id', $lastinsert_id)->update(array('heading_id' =>$x));
//   }
    if($id==$last_id){

      $key = Keypairs::where('heading_id', $last_id)->update(array('heading_id' =>$lastinsert_id));
      }
      //to update the next child parent id incase of a new insertion
      else{
      $key = KeypairHeadings::where('parent_id','=', $id)->where('id', '!=', $lastinsert_id)->update(array('parent_id' =>$lastinsert_id));
      }

      }
          }
      return response()->json(['success'=>"data updated"],$this->sucessStatus);
    }
}
