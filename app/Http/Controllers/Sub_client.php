<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Sub_clients;
use App\User;
use App\Http\Resources\Sub_client as SubclientResource;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;



class Sub_client extends Controller
{


    public $sucessStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get Sub_clients
        $p_id=Auth::user()->id;

        $clients_json = [];
        $subclients_json=[];
        $clients =   Sub_clients::where('parent_id', '=' ,$p_id)->get();

    foreach ($clients as $client ) {

      $clients_json[]=
      // Access $client->sku here...

      [
        'name' => $client->name,
        'email' => $client->email,
        'title' => $client->type,
        'status' => $client->status,
        'location' => $client->location,
        'address' => $client->address,
        'parent_id' => $client->parent_id,
        'url' => '/subclient/' . $client->id,
        'id'=>$client->id
        ];


    }

             return response()->json(['clients' => $clients_json], $this->sucessStatus);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //to store Sub_client
        $validator = Validator::make($request->all(),[
             'email'  => 'required|unique:users',
             'contact'=> 'required|unique:users',
              'password' => 'required',
             'c_password' => 'required|same:password',
        ]);

        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()],401);
        }



        $subclient = new Sub_clients;
        $subclient->contact = $request->input('contact');
        $subclient->password = bcrypt($request->input('password'));
        $subclient->address = $request->input('address');
        $subclient->type = $request->input('type');
        //$subclient->parent_id = Auth::user()->id;
        $subclient->status = 1;
        $subclient->email = $request->input('email');
        $subclient->location = $request->input('location');
        $subclient->name = $request->input('name');
        $subclient->user_id = Auth::user()->id;
        $subclient->parent_id = $request->input('parent_id');
        $subclient->save();
        return response()->json(['success' =>"New User Registered"], $this->sucessStatus);




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        if($id==="1") {
            return response()->json(['warning'=>"access denied"],404);

        }else{

          //Get single client
          $subclient = Sub_clients::findorfail($id);
          //Return single client data
          return new SubclientResource($subclient);

        }



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->input('sub_id');
      // $id=$request->input('item');

        $subclient = $request->isMethod('put') ? Sub_clients::findOrFail($id) : new Sub_clients;


      //contact check
      $contact=$request->input('contact');
      if($contact==""){

      }
      else{
        $validator = Validator::make($request->all(),[
                         'contact'      => 'required|unique:users',
        ]);

        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()],401);
        }
      $client = Sub_clients::where('id', $id)->update(array('contact' =>$contact));
        return response()->json(['success' => $contact], $this->sucessStatus);
      }



      //password check
      $pass=$request->input('password');
      if($pass==""){

      }
      else{
        // $client->password = $request->input('password');
        $client = Sub_clients::where('id', $id)->update(array('password' =>bcrypt($pass)));
      }


      //address check
      $address=$request->input('address');
      if($address==""){

      }
      else{
        // $client->address = $request->input('address');
          $client = Sub_clients::where('id', $id)->update(array('address' =>$address));
            return response()->json(['success' => $address], $this->sucessStatus);
      }

      //email check
      $email=$request->input('email');
      if($email==""){

      }
      else{
        //validation
                  $validator = Validator::make($request->all(),[
                       'email' => 'required|unique:users',
                  ]);

                  if($validator->fails()) {
                      return response()->json(['error' => $validator->errors()],401);
                  }
        // $client->email = $request->input('email');
          $client = Sub_clients::where('id', $id)->update(array('email' =>$email));
          return response()->json(['success' => $email], $this->sucessStatus);

      }

      //location check
      $loca=$request->input('location');

      if($loca==""){

      }
      else{
        // $client->locn = $request->input('location');
          $client = Sub_clients::where('id', $id)->update(array('location' =>$loca));
          return response()->json(['success' => $loca], $this->sucessStatus);
      }

      //username check
      $name=$request->input('name');
      if($name==""){

      }
      else{
        // $client->username = $request->input('username');
        $client = Sub_clients::where('id', $id)->update(array('name' =>$name));
        return response()->json(['success' => $name], $this->sucessStatus);

      }

      //user_id check
      $user_id=$request->input('user_id');
      if($user_id==""){

      }
      else{
        // $client->user_id = $request->input('user_id');
        $client = Sub_clients::where('id', $id)->update(array('user_id' =>$user_id));
        return response()->json(['success' => $user_id], $this->sucessStatus);
      }

      //type check
      $type=$request->input('type');
      if($type==""){

      }
      else{
        // $client->c_id = $request->input('c_id');
        $client = Sub_clients::where('id', $id)->update(array('type' =>$type));
        return response()->json(['success' => $type], $this->sucessStatus);
      }

      $status=$request->input('status');
      if($status==""){

      }
      else{
        // $client->c_id = $request->input('c_id');
        $client = Sub_clients::where('id', $id)->update(array('status' =>$status));
        return response()->json(['success' => $status], $this->sucessStatus);
      }

    // return  SubclientResource::collection($client);

    }
    
    
     public function update_all(Request $request)
    {
   
        $subclient = new Sub_clients;
        $id = $request->input('sub_id');
        $contact = $request->input('contact');
        // $subclient->password = bcrypt($request->input('password'));
        $address = $request->input('address');
        // $subclient->type = $request->input('type');
        //$subclient->parent_id = Auth::user()->id;
        
        // $subclient->email = $request->input('email');
        $location = $request->input('location');
        $name = $request->input('name');
        // $subclient->user_id = Auth::user()->id;
        // $subclient->parent_id = $request->input('parent_id');
      $client = Sub_clients::where('id', $id)->update(array('name'=>$name,'contact'=>$contact,'address'=>$address,'location'=>$location));
        return response()->json(['success' =>" User updated"], $this->sucessStatus);

    }
    
    
    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       if($id==="1") {
          return response()->json(['warning'=>"access denied"],404);
        }


        //destroy single client
         $subclient = Sub_clients::findorfail($id);
      if($subclient->delete()){

          return new SubclientResource($subclient);

      }
    }

//Get subclients
public function clients_sub($id){

  if($id==1){
    return response()->json(['warning'=>"access denied"],404);
  }
  else{
    $sub_clients=[];
    $subclients = Sub_clients::where('parent_id', '=', $id)->get();
    foreach ($subclients as $client) {
    $sub_clients[]=
    [
      'id' => $client->id,
      'name' => $client->name,
      'email' => $client->email,
      'title' => $client->type,
      'status' => $client->status,
      'location' => $client->location,
      'address' => $client->address,
      'parent_id' => $client->parent_id,
      'url' => '/subclient/' . $client->id

    ];
    }
    return response()->json(['clients'=>$sub_clients],200);
  }
}

public function logout() {
      $accessToken = Auth::user()->token();
      
      echo $accessToken;exit;
      

      DB::table('oauth_refresh_tokens')
          ->where('access_token_id', $accessToken->id)
          ->update([
              'revoked' => true
          ]);
          
          

      $accessToken->revoke();
    
    
    
      return response()->json(['message' => 'signed Out'],200);
  }

}
