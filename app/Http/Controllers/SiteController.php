<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\site;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //select all Sites
    $Sites = site::all();
    return response()->json([$Sites], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validator = Validator::make($request->all(),[
      'site_name' =>'required',
      'user' =>'required|exists:users,id',
      ]);

      if($validator->fails()){
        return response()->json(['eror',$validator->errors()],401);
      }
      $site =new site;
      $site->site_name = $request->input('site_name');
      $site->user = $request->input('user');
      $user = Auth::user();
      $user_id = $user->id;

      $site->user_id = $user_id;

      $site->save();
      return response()->json([$site],200);
      }

    /**
     * Display site details of a particular user
     *
     * @param  \App\site  $site
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $site   = site::where('user',$id)->get();
      //if id is not found
      if($site->isEmpty()) {
        return response()->json(['error'=>"id not found"],404);
}
//valid response
    return response()->json([$site]);

   }

   public function show_id($id)
   {
     $site   = site::where('id',$id)->get();
     //if id is not found
     if($site->isEmpty()) {
       return response()->json(['error'=>"id not found"],404);
}
//valid response
   return response()->json([$site]);

  }
/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            $this->validate($request, [
                'site_name' => 'required',
            ]);

            try{
            $site= site::findorfail($id);
          }
          catch (ModelNotFoundException $exception) {
           return response()->json(['error'=>"id not found"],404);
        }
            $site->site_name = $request->site_name;
            $site->save();

            return response()->json($site);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    try {
      $site = site::findOrFail($id);
    } catch (ModelNotFoundException $exception) {
      return response()->json(['error'=>"id not found"],404);
    }

    $site->delete();
    return response()->json([$site]);

    }
}
