<?php
declare(strict_types=1);
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection as Collection;
use Illuminate\Http\Request;
use App\Access_tables;
use App\Keypairs;
use App\Type;
use App\Asset;
use App\User;
use DB;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
use ParagonIE\Halite\Password;
use ParagonIE\Halite\Halite;
use ParagonIE\Halite\Asymmetric\{
    Crypto as Asymmetric,
    EncryptionPublicKey,
    EncryptionSecretKey
};
use ParagonIE\Halite\Alerts as CryptoException;
use Validator;
use App\Http\Resources\Access_table as AccesstableResource;

class AccesstableController extends Controller
{
    public $sucessStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //get Access results
         $user = Auth::user();
         $user_id=$user->id;
         $accessdata =Access_table::where('user_id',$user_id)->all();
         return response()->json(['success'=>$accessdata],$this->sucessStatus);

     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         $user = Auth::user();
         $accessdata = new Access_tables;

         $accessdata->keypair_id = $request->input('keypair_id');
        //  $accessdata->group_id = $request->input('group_id');
         $accessdata->access_permission = $request->input('access_permission');
         $accessdata->permission_user_id = $request->input('permission_user_id');
         $accessdata->site_id = $request->input('site_id');
         $accessdata->user_id = $user->id;


         if($accessdata->save()){
           return new AccesstableResource($accessdata);
         }

     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show(Request $request)
     {

      $id=null;
      $group_id=null;
      $keypair_id=null;
      $user_id=null;

         $id             = $request->input('id');
         $group_id       = $request->input('group_id');
         $keypair_id     = $request->input('permission_user_id');
         $key_id         = $request->input('keypair_id');

         $user_id        = $request->input('user_id');
         //Get single client

         //request using user id
                    if($id==null&&$group_id==null&&$keypair_id==null&&$key_id==null){

                    $accessdata = Access_tables::where('user_id',$user_id)
                    ->where(function($q) {
                    $q->where('access_permission', '700')
                    ->orWhere('access_permission', '600')
                    ->orWhere('access_permission', '400');
     })
     ->get();

                    }
                    //request using permission_user_id
                    elseif ($id==null&&$group_id==null&&$user_id==null&&$key_id==null) {

                    $accessdata = Access_tables::where('permission_user_id',$keypair_id)
                    ->where(function($q) {
                    $q->where('access_permission', '700')
                    ->orWhere('access_permission', '600')
                    ->orWhere('access_permission', '400');
     })
     ->get();


         }

         //request using Keypair id
         elseif ($id==null&&$group_id==null&&$user_id==null&&$keypair_id==null) {

         $accessdata = Access_tables::where('keypair_id',$key_id)
         ->where(function($q) {
         $q->where('access_permission', '700')
         ->orWhere('access_permission', '600')
         ->orWhere('access_permission', '400');
})
->get();


}
                  //request using Group id
                  elseif ($id==null&&$keypair_id==null&&$user_id==null&&$key_id==null) {
                  $accessdata = Access_tables::where('group_id','=',$group_id)
                  ->where(function($q) {
                  $q->where('access_permission', '700')
                  ->orWhere('access_permission', '600')
                  ->orWhere('access_permission', '400');
   })
   ->get();


         }
                  //Request using id
                  elseif ($group_id==null&&$keypair_id==null&&$user_id==null&&$key_id==null) {
                   $accessdata = Access_tables::where('id',$id)
                   ->where(function($q) {
                   $q->where('access_permission', '700')
                   ->orWhere('access_permission', '600')
                   ->orWhere('access_permission', '400');
    })
    ->get();



                }


         //Return single client data
        return response()->json(['success'=>$accessdata],$this->sucessStatus);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request)
     {
       $user = Auth::user();
       $user_id =$user->id;

        $id=$request->input('id');
       //$access_permission check
       $access_permission=$request->input('access_permission');
       if($access_permission==""){

       }
       else{

         $key = Access_tables::where('id', $id)->where('user_id', $user_id)->update(array('access_permission' =>$access_permission));
           // return new AccesstableResource($key);
           return response()->json(['success'=>$key],$this->sucessStatus,$headers=[], $options=JSON_PRETTY_PRINT);
       }

       //user_id check
       $permission_user_id = $request->input('permission_user_id');
       if($permission_user_id==""){

       }
       else{
         // $client->user_id = $request->input('user_id');
         $key = Access_tables::where('id', $id)->where('user_id', $user_id)->update(array('permission_user_id' =>$permission_user_id));
         return response()->json(['success'=>$key],$this->sucessStatus,$headers=[], $options=JSON_PRETTY_PRINT);
       }

            // return new AccesstableResource($key);
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
      //  $id=$request->input('id');
        $user = Auth::user();
        $user_id =$user->id;

         //destroy single client
         $accessdata = Access_tables::where('id', $id)->where('user_id', $user_id)->firstOrFail();
               if($accessdata->delete()){
           return new AccesstableResource($accessdata);

       }

     }

     public function decrypt(Request $request){



      $user = $request->input('user');
      $key_value = $request->input('keypair_value');
      $permission = $request->input('access_permission');

      $validator = Validator::make($request->all(),
      [
           'user'  => 'required',
           'keypair_value'=> 'required',
           'access_permission'=> 'required',
      ]);

      if($validator->fails()) {
          return response()->json(['error' => $validator->errors()],401);
      }


      if($permission==="600"||$permission==="700"||$permission==="400"){


       try {
           // First, manage the keys
             if (!file_exists("$user-sk.key")) {
               $ciphertext= "Access Denied !";
               return response()->json(['success' => $ciphertext], 401);
               exit(127);
             }
               else{

             $secretKey = KeyFactory::loadEncryptionKey("$user-sk.key");
             $data = $key_value;
             $ciphertext = Symmetric::decrypt($data, $secretKey);
             $decrypted_data =$ciphertext->getString();
               }

       } catch (\ParagonIE\Halite\Alerts\HaliteAlert $e) {
        // Oh no!
                 $ciphertext= "Access denied... !";
                 return response()->json(['success' => $ciphertext], 401);
                 exit(127);
     }

     return response()->json(['success' => $decrypted_data],200);
   }else{

     $ciphertext= "Permission denied... !";
     return response()->json(['success' => $ciphertext], 401);
     exit(127);
   }
     }

     public function distinctGroupid(Request $request){

       //get Access results
       $user = Auth::user();
       $permission_user_id = $request->input('permission_user_id');

       // $accessdata =Access_tables::distinct('group_id')->where('user_id',$user_id)->pluck('group_id');


  $accessdata = Access_tables::where('permission_user_id',$permission_user_id)->select('keypair_id','site_id','permission_user_id','user_id','access_permission')
       ->where(function($q) {
       $q->where('access_permission', '700')
       ->orWhere('access_permission', '600')
       ->orWhere('access_permission', '400');
})->get();
       // dd($accessdata);
        return response()->json(['success'=>$accessdata],$this->sucessStatus);
     }


}
