<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Redirect;
use Session;
use Storage;

class DeploySettingsController extends Controller
{
    public function install()
    {

        $domains[] = Input::only('url');
        foreach ($domains as $data) {
            $url = $data['url'];
        }
        //trimed the url for slash on last
        $validation = false;
        /*Parse URL*/
        $urlparts = parse_url(filter_var($url, FILTER_SANITIZE_URL));
        /*Check host exist else path assign to host*/
        if (!isset($urlparts['host'])) {
            $urlparts['host'] = $urlparts['path'];
        }

        if ($urlparts['host'] != '') {
            /*Add scheme if not found*/
            if (!isset($urlparts['scheme'])) {
                $urlparts['scheme'] = 'http';
            }
            /*Validation*/
            if (checkdnsrr($urlparts['host'], 'A') && in_array($urlparts['scheme'], array('http', 'https')) && ip2long($urlparts['host']) === false) {
                $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
                $url = $urlparts['scheme'] . '://' . $urlparts['host'] . "/";

                if (filter_var($url, FILTER_VALIDATE_URL) !== false && @get_headers($url)) {
                    $validation = true;
                }
            }
        }

        if (!$validation) {
            echo "Its Invalid Domain Name.";

        } else {

            $urlin = rtrim($url, "/");
            Session::put('domain', $urlin);
            echo $url_session = Session::get('domain');
            $bool = Storage::disk('local')->put('env.js', "
     window._env = {
         backendUrl: '" . $url_session . "/public/api/',
     };
     ");

            $redirect_url = URL::to('/install');
            return Redirect::to($redirect_url);
        }

    }
}
