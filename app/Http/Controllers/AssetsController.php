<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Asset;
use App\type;
use DB;
use Validator;
class AssetsController extends Controller
{
  public $sucessStatus = 200;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();
    $user_id =$user->id;
    //select all types
    $assets= Asset::all();
    // $assets= DB::table('assets')
    // ->select('assets.*','product_name as type')
    // ->join('types','types.id','=','assets.type_id')
    //  ->where('assets.user_id','=', $user_id)
    // ->get();
    return response()->json(['success' => $assets], $this->sucessStatus);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $user = Auth::user();
    $validator = Validator::make($request->all(),[
         'asset_name' => 'required|unique:assets',
    ]);

    if($validator->fails()) {
        return response()->json(['error' => $validator->errors()],401);
    }

    $asset = new Asset;
    $asset->asset_name = $request->input('asset_name');
    //$asset->type_id = $request->input('type_id');
    $asset->user_id = $user->id;
    $asset->save();
    return response()->json(['success'=>$asset],$this->sucessStatus);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

      $asset= Asset::findorfail($id);
      // $asset= DB::table('assets')
      // ->join('types','types.id','=','assets.type_id')
      // ->select('assets.*','product_name as type')
      // ->where('assets.id','=',$id)
      // ->get();
      return response()->json(['success'=>$asset],$this->sucessStatus);

  }

  public function showby_type($id)
  {

    {
      $assetsby_type   = Asset::where('type_id',$id)->get();
      //if id is not found
      if($assetsby_type->isEmpty()) {
        return response()->json(['error'=>"id not found"],404);
}
//valid response
    return response()->json([$assetsby_type]);

   }


  }

    public function showby_user($id)
  {

    {
      $assetsby_type   = Asset::where('user_id',$id)->get();
      //if id is not found
      if($assetsby_type->isEmpty()) {
        return response()->json(['error'=>"id not found"],404);
}
//valid response
    return response()->json([$assetsby_type]);

   }

 }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $id=$request->input('asset_id');
      //
      $asset = $request->isMethod('put') ? Asset::findOrFail($id) : new Asset;

      $asset_name=$request->input('asset_name');
      if($asset_name==""){

      }
      else{
        $validator = Validator::make($request->all(),[
             'asset_name' => 'required|unique:assets',
        ]);

        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()],401);
        }

      $asset = Asset::where('id', $id)->update(array('asset_name' =>$asset_name));
      return response()->json(['success' => $asset_name], $this->sucessStatus);
      }

      $user_id=$request->input('user_id');
      if($user_id==""){

      }
      else{
      $asset = Asset::where('id', $id)->update(array('user_id' =>$user_id));
      return response()->json(['success' => $user_id], $this->sucessStatus);
      }

      // $type_id=$request->input('type_id');
      // if($type_id==""){
      //
      // }
      // else{
      // $asset = Asset::where('id', $id)->update(array('type_id' =>$type_id));
      // return response()->json(['success' => $type_id], $this->sucessStatus);
      // }
  }



  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $asset = Asset::findorfail($id);
      if($asset->delete()){
      return response()->json(['success' => $asset], $this->sucessStatus);

   }
  }


}
