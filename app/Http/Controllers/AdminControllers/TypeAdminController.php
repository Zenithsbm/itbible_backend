<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Type;
use Validator;

class TypeAdminController extends Controller
{
  public $sucessStatus = 200;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //select all types
    $Types = Type::paginate(15);
    return response()->json(['success' => $Types], $this->sucessStatus);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),[
         'product_name' => 'required|unique:types',
    ]);

    if($validator->fails()) {
        return response()->json(['error' => $validator->errors()],401);
    }

    $type = new Type;

    $type->product_name = $request->input('product_name');
    $type->user_id = Auth::user()->id;
    $type->save();
    return response()->json(['success'=>$type],$this->sucessStatus);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
      $type= Type::findorfail($id);
      return response()->json(['success'=>$type],$this->sucessStatus);

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
      $id=$request->input('type_id');
      //
      $type = $request->isMethod('put') ? Type::findOrFail($id) : new Type;
      $product_name=$request->input('product_name');
      if($product_name==""){

      }
      else{
        //validation
                  $validator = Validator::make($request->all(),[
                       'product_name' => 'required|unique:types',
                  ]);

                  if($validator->fails()) {
                      return response()->json(['error' => $validator->errors()],401);
                  }

      $type = Type::where('id', $id)->update(array('product_name' =>$product_name));
      return response()->json(['success' => $product_name], $this->sucessStatus);
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy()
  {
      $id=$request->input('type_id');
      //
      $subclient = Type::findorfail($id);
      if($subclient->delete()){
      return response()->json(['success' => $subclient], $this->sucessStatus);

   }
  }

}
