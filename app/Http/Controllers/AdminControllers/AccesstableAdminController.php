<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection as Collection;
use Illuminate\Http\Request;
use App\Access_tables;
use App\Keypairs;
use App\Type;
use App\Asset;
use App\User;
use DB;
use App\Http\Resources\Access_table as AccesstableResource;


class AccesstableAdminController extends Controller
{
  public function __construct(){

     $this->middleware('IsAdmin');
 }

  public $sucessStatus = 200;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function index()
   {
       //get Access results
       $accessdata = DB::table('access_tables')
       ->join('keypairs','keypairs.id','=','access_tables.keypair_id')
       ->join('users','users.id','=','access_tables.user_id')
       ->join('assets','assets.id','=','keypairs.asset_id')
       ->join('types','types.id','=','assets.type_id')
       ->select('assets.*','asset_name as type','keypairs.*','users.name as username','types.*','product_name as type')
       // ->where('access_tables.user_id','=',$id)
       ->get();

        return response()->json(['success'=>$accessdata],$this->sucessStatus,$headers=[], $options=JSON_PRETTY_PRINT);

   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $user = Auth::guard('api')->user();
       $accessdata = new Access_tables;
       $accessdata->keypair_id = $request->input('keypair_id');
       $accessdata->access_permission = $request->input('access_permission');
       $accessdata->user_id = $request->input('user_id');

       if($accessdata->save()){
         return new AccesstableResource($accessdata);
       }

   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //Get single client
       $user =Auth::guard('api')->user();
       $accessdata = Access_tables::findorfail($id);
       $accessdata= DB::table('access_tables')
       ->join('keypairs','keypairs.id','=','access_tables.keypair_id')
       ->join('users','users.id','=','access_tables.user_id')
       ->join('assets','assets.id','=','keypairs.asset_id')
       ->join('types','types.id','=','assets.type_id')
       ->select('assets.*','asset_name as type','keypairs.*','users.name as username')
       ->where('access_tables.id','=',$id)
       // ->where('access_tables.user_id','=',$user->id)
       ->get();

       //Return single client data
       return new AccesstableResource($accessdata);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {

      $id=$request->input('access_id');
     //$access_permission check
     $access_permission=$request->input('access_permission');
     if($access_permission==""){

     }
     else{

       $key = Access_tables::where('id', $id)->update(array('access_permission' =>$access_permission));
         // return new AccesstableResource($key);
         return response()->json(['success'=>$access_permission],$this->sucessStatus,$headers=[], $options=JSON_PRETTY_PRINT);
     }

     //user_id check
     $user_id=$request->input('user_id');
     if($user_id==""){

     }
     else{
       // $client->user_id = $request->input('user_id');
       $key = Access_tables::where('id', $id)->update(array('user_id' =>$user_id));
     }

          // return new AccesstableResource($key);
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy(Request $request)
   {

      $id=$request->input('access_id');
      $user = Auth::guard('api')->user();
      $user_id =$user->id;
       //
       //destroy single client
       $accessdata = Access_tables::where('id', $id)->firstOrFail();
             if($accessdata->delete()){
       return new AccesstableResource($accessdata);

     }

   }

          //Update the delete flag
      
}
