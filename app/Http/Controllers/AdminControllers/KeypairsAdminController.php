<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Keypairs;
use App\Asset;
use App\Type;
use App\Http\Resources\Keypair as KeypairResource;
use DB;

class KeypairsAdminController extends Controller
{
  public $sucessStatus = 200;
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
 public function index()
 {
     //get Keypairs
     $user = Auth::guard('api')->user();
     $keypair= DB::table('keypairs')

     ->join('assets','assets.id','=','keypairs.asset_id')
     ->join('users','users.id','=','keypairs.user_id')
     ->join('types','types.id','=','assets.type_id')
     // ->select('users.*','name as Username','keypairs.*')
     ->select('assets.*','asset_name as type','keypairs.*','users.name as Keypair_to')
     // ->where('keypairs.user_id','=',$user->id)
     ->get();
     return response()->json(['success'=>$keypair],$this->sucessStatus);
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
     $keypair = new Keypairs;
     $keypair->heading = $request->input('heading');
     $keypair->asset_id = $request->input('asset_id');
     $keypair->keypair_name = $request->input('keypair_name');
     $keypair->keypair_value = $request->input('keypair_value');
     // $keypair->user_id = $request->input('user_id');
     $user = Auth::guard('api')->user();
     $keypair->user_id = $user->id;
     if($keypair->save()){
       return response()->json(['success'=>$keypair],$this->sucessStatus);

     }

 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {
     //Get single keypair, no scope for primary id
     $keypair= Asset::findorfail($id);
     $keypair= DB::table('keypairs')

     ->join('assets','assets.id','=','keypairs.asset_id')
     ->join('types','types.id','=','assets.type_id')
     ->select('assets.*','product_name as type','keypairs.*')
     ->where('assets.id','=',$id)
     // ->where('assets.id','=',$id)
     ->get();
     return response()->json(['success'=>$keypair],$this->sucessStatus);
 }



 //return particular properties only for specific item
      public function showpairby_user()
      {
     $user = Auth::guard('api')->user();
     $keypair= DB::table('keypairs')
     ->join('assets','assets.id','=','keypairs.asset_id')
     ->join('types','types.id','=','assets.type_id')
     ->select('assets.*','product_name as type','keypairs.*')
     ->where('keypairs.user_id', '=',$user->id)
     ->get();


          //Return user keypair client data
       return response()->json(['success'=>$keypair],$this->sucessStatus);
      }



 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request)
 {
   $id=$request->input('key');

   $messege="Updated";

   //$keypair_name check
   $keypair_name=$request->input('keypair_name');
   if($keypair_name==""){

   }
   else{
   $key = Keypairs::where('id', $id)->update(array('keypair_name' =>$keypair_name));
   }

   $keypair_value=$request->input('keypair_value');
   if($keypair_value==""){

   }
   else{
   $key = Keypairs::where('id', $id)->update(array('keypair_value' =>$keypair_value));
   }


   //user_id check
   $user_id=$request->input('user_id');
   if($user_id==""){

   }
   else{
     // $client->user_id = $request->input('user_id');
     $key = Keypairs::where('id', $id)->update(array('user_id' =>$user_id));
   }

   //item_id check
   $heading=$request->input('heading');
   if($heading==""){

   }
   else{
     // $client->user_id = $request->input('user_id');
     $key = Keypairs::where('id', $id)->update(array('heading' =>$heading));
   }
    return response()->json(['success'=>$messege],$this->sucessStatus);
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */



 public function destroy(Request $request,$id)
{
$user = Auth::guard('api')->user();
$user_id = $user->id;

$keypair = Keypairs::where('id', $id)->where('user_id', $user_id)->firstOrFail();
if($keypair->delete()){
return response()->json(['success' => $keypair], $this->sucessStatus);
}
}


}
