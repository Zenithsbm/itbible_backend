<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Type;
use Validator;

class TypeController extends Controller
{
    public $sucessStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /*select all types */

      //$Types = Type::where('user_id','=',Auth::user()->id)->paginate(15);
      $Types = Type::all();
      return response()->json(['success' => $Types], $this->sucessStatus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(),[
           'product_name' => 'required|unique:types',
      ]);

      if($validator->fails()) {
          return response()->json(['error' => $validator->errors()],401);
      }
      $type = new Type;
      $type->product_name = $request->input('product_name');
      $user = Auth::user();
      $user_id=$user->id;

      $type->user_id = $user_id;

      // $type->user_id = auth()->id;
      $type->save();
      return response()->json(['success'=>$type],$this->sucessStatus);
    }

    /**
     * Display the specified Type by id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $type= Type::findorfail($id);
        return response()->json(['success'=>$type],$this->sucessStatus);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->input('type_id');
        $type = $request->isMethod('put') ? Type::findOrFail($id) : new Type;
        $product_name=$request->input('product_name');
        if($product_name==""){
  return response()->json(['error' => "Product name required"],404);
        }
        else{
          //validation
                    // $validator = Validator::make($request->all(),[
                    // 'product_name' => 'required|unique:types',
                    // ]);
                    //
                    // if($validator->fails()) {
                    //     return response()->json(['error' => $validator->errors()],401);
                    // }

        $type = Type::where('id', $id)->update(array('product_name' =>$product_name));
        return response()->json(['success' => $product_name], $this->sucessStatus);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subclient = Type::findorfail($id);
        if($subclient->delete()){
        return response()->json(['success' => $subclient], $this->sucessStatus);

     }
    }


}
