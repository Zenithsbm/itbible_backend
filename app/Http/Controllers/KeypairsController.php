<?php
declare(strict_types=1);
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Keypairs;
use App\Asset;
use App\Type;
use App\Http\Resources\Keypair as KeypairResource;
use DB;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
use ParagonIE\Halite\Password;
use ParagonIE\Halite\Halite;
use ParagonIE\Halite\Asymmetric\{
    Crypto as Asymmetric,
    EncryptionPublicKey,
    EncryptionSecretKey
};
use ParagonIE\Halite\Alerts as CryptoException;




class KeypairsController extends Controller
{
      public $sucessStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //get Keypairs
          $user = Auth::user();
          $keypair= DB::table('keypairs')
         ->where('keypairs.user_id','=',$user->id)
         ->get();

         return response()->json(['success'=>$keypair],$this->sucessStatus);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
      public function store(Request $request)
     {

         $array = $request->all();
       foreach($array["data"] as $row)
          {

            $asset_id = array();
            $keypair_name = array();
            $keypair_value = array();
            $user = Auth::user();
            $user_id=$user->id;
            
          

          //insertion of keypair data

                                  $keypair =   keypairs::create([
                                      
                                      
                                 'keypair_name'   => $row["keypair_name"],
                                 'user_id'        => $user_id,
                                 'asset_id'       =>$row["asset_id"],
                                 'keypair_value'  =>$row["keypair_value"] ,

             ]);

               }

         if($keypair->save()){
           return response()->json(['success'=>$keypair],$this->sucessStatus);

         }


     }
     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show(Request $request)
     {
         //Get single keypair
         $id           =  null;
         $asset_id   =  null;
         $user_id      =  null;

         $id           =  $request->input('id');
         $asset_id  =  $request->input('asset_id');
         $user_id      =  $request->input('user_id');
         $user         = Auth::user();

if($asset_id==null&&$user_id==null){

  $key_pair   = Keypairs::findorfail($id);

}elseif ($id==null&&$user_id==null) {

    $key_pair   = Keypairs::where('asset_id',$asset_id)->where('user_id',$user->id)->get();

}elseif ($id==null&&$asset_id==null) {

$key_pair   = Keypairs::where('user_id',$user_id)->get();

}elseif ($id==null) {
  // code...
  $key_pair   = Keypairs::where('asset_id',$asset_id)->where('user_id',$user_id)->get();
}
// dd($key_pair);
         return response()->json(['success'=>$key_pair],$this->sucessStatus);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request)
     {
       $id=$request->input('id');

       $messege="Updated";

       //$keypair_name check
       $keypair_name=$request->input('keypair_name');
       if($keypair_name==""){

       }
       else{
       $key = Keypairs::where('id', $id)->update(array('keypair_name' =>$keypair_name));
       }

       $keypair_value=$request->input('keypair_value');

              if($keypair_value==""){

       }
       else{
       $key = Keypairs::where('id', $id)->update(array('keypair_value' =>$keypair_value));
       }


       //user_id check
       $user_id=$request->input('user_id');
       if($user_id==""){

       }
       else{
         // $client->user_id = $request->input('user_id');
         $key = Keypairs::where('id', $id)->update(array('user_id' =>$user_id));
       }

       //item_id check
       $heading=$request->input('asset_id');
       if($heading==""){

       }
       else{
         // $client->user_id = $request->input('user_id');
         $key = Keypairs::where('id', $id)->update(array('asset_id' =>$heading));
       }
        return response()->json(['success'=>$messege],$this->sucessStatus);
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */



     public function destroy($id)
{
    //$id=$request->input('id');
    $user = Auth::user();
    $user_id = $user->id;
    $keypair = Keypairs::where('id', $id)->where('user_id', $user_id)->firstOrFail();
    if($keypair->delete()){

      return response()->json(['success' => $keypair], $this->sucessStatus);

  }
}




}