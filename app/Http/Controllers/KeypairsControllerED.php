<?php
declare(strict_types=1);
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Keypairs;
use App\Asset;
use App\Type;
use App\Http\Resources\Keypair as KeypairResource;
use DB;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
use ParagonIE\Halite\Password;
use ParagonIE\Halite\Halite;
use ParagonIE\Halite\Asymmetric\{
    Crypto as Asymmetric,
    EncryptionPublicKey,
    EncryptionSecretKey
};
use ParagonIE\Halite\Alerts as CryptoException;




class KeypairsControllerED extends Controller
{
      public $sucessStatus = 200;

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

         $array = $request->all();
       foreach($array["data"] as $row)
          {

            $heading_id = array();
            $keypair_name = array();
            $keypair_value = array();
            $user = Auth::user();
            $user_id=$user->id;
            $key_value=$row["keypair_value"];
            $e_data = $this->encrypt($key_value);
          //insertion of keypair data

                                  $keypair =   keypairs::create([
                                 'heading_id'     => $row["heading_id"],
                                 'keypair_name'   => $row["keypair_name"],
                                 'user_id'        => $user_id,
                                 'keypair_value'  =>$e_data ,

             ]);

               }

         if($keypair->save()){
           return response()->json(['success'=>$keypair],$this->sucessStatus);

         }
     }


     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request)
     {
       $id=$request->input('id');

       $messege="Updated";

       //$keypair_name check
       $keypair_name=$request->input('keypair_name');
       if($keypair_name==""){

       }
       else{
       $key = Keypairs::where('id', $id)->update(array('keypair_name' =>$keypair_name));
       }

       $keypair_value=$request->input('keypair_value');
       $key_value= $keypair_value;
       $keypair_value = $this->encrypt($key_value);
       if($keypair_value==""){

       }
       else{
       $key = Keypairs::where('id', $id)->update(array('keypair_value' =>$keypair_value));
       }


       //user_id check
       $user_id=$request->input('user_id');
       if($user_id==""){

       }
       else{
         // $client->user_id = $request->input('user_id');
         $key = Keypairs::where('id', $id)->update(array('user_id' =>$user_id));
       }

       //item_id check
       $heading=$request->input('heading_id');
       if($heading==""){

       }
       else{
         // $client->user_id = $request->input('user_id');
         $key = Keypairs::where('id', $id)->update(array('heading_id' =>$heading));
       }
        return response()->json(['success'=>$messege],$this->sucessStatus);
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */

public function encrypt($key_value){
  $user = Auth::user();
  $user_id = $user->id;
  $user =$user_id;

  try {
      // First, manage the keys
      if (!file_exists("$user-sk.key")) {
          $secretKey = KeyFactory::generateEncryptionKey();
          KeyFactory::save($secretKey, "$user-sk.key");
      } else {
          $secretKey = KeyFactory::loadEncryptionKey("$user-sk.key");
      }
      $data = new HiddenString($key_value);
      $ciphertext = Symmetric::encrypt($data, $secretKey);

      }  catch (\ParagonIE\Halite\Alerts\HaliteAlert $e) {
   // Oh no!
   $ciphertext= "Access denied !";
   return response()->json(['success' => $ciphertext], 401);
   exit(127);
      }

          return $ciphertext;
      }


public function decrypt($key_value){
  $user = Auth::user();
  $user_id = $user->id;
  $user =$user_id;

  try {
      // First, manage the keys
        if (!file_exists("$user-sk.key")) {
          $ciphertext= "Access Denied for user!";
          return response()->json(['success' => $ciphertext], 401);
          exit(127);
        }
          else{

        $secretKey = KeyFactory::loadEncryptionKey("$user-sk.key");
        $data = $key_value;
        $ciphertext = Symmetric::decrypt($data, $secretKey);
        $decrypted_data =$ciphertext->getString();
          }

  } catch (\ParagonIE\Halite\Alerts\HaliteAlert $e) {
   // Oh no!
            $ciphertext= "Access denied... !";
            return response()->json(['success' => $ciphertext], 401);
            exit(127);
}

return response()->json(['success' => $decrypted_data],200);
}


}
