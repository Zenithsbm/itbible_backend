<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Socialite;
use SocialiteProviders\Manager\SocialiteWasCalled;


class PassportController extends Controller
{
    public $sucessStatus = 200;

    /*
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function login() {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->sucessStatus);
        }
        else {
            return response()->json(['error' => 'Invalid username or Password'], 401);
        }
    }

    /*
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {

        $validator = Validator::make($request->all(),[
             'name' => 'required|unique:users',
             'email' => 'required|email|unique:users',
             'password' => 'required',
             'c_password' => 'required|same:password',
             'parent_id' => 'required',
             'type' => 'required',
             'status' => 'required',
             'contact' => 'required|unique:users',
             'location' => 'required',
             'address' => 'required',
             'user_id' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()],401);
        }

        $input = $request->all();
        dd($input);
         // print_r ($input);exit;
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return response()->json(['success' => $success], $this->sucessStatus);
    }

    /*
     * details api
     *
     * @return \Illumiante\Http\Response
     */
     public function getDetails() {
         $user = Auth::user();
         return response()->json(['success' => $user], $this->sucessStatus);
     }

     public function redirectToProvider($provider)
   {
       return Socialite::driver($provider)->stateless()->redirect();
   }

   /**
    * Obtain the user information from GitHub.
    *
    * @return Response
    */
   public function handleProviderCallback($provider)
   {
         $user = Socialite::driver($provider)->stateless()->user();
         $authUser = $this->findorCreateUser($user, $provider);

        // return $user->token;
        return    $authUser;

   }

   public function findorCreateUser($user, $provider){

      $authUser = User::where('email', $user->email)->first();
      if($authUser){
        return $authUser;
      }
      return User::create([
        'name'    => $user->name,
          'email'    => $user->email,
            'status'    => 1,
              'type'    => 'user',
                'provider'    => strtoupper($provider),
                  'provider_id'    => $user->id,
                    'location'    => 0,
                        'password'    => '123456789',
                        'contact'    => '123456789',
                          'address'    => 'padannamakakak',
      ]);
   }

}
