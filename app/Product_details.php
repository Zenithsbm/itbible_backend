<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

class Product_details extends Model implements AuditableContract
{
  use Auditable;
  use \OwenIt\Auditing\Auditable;
    //
}
