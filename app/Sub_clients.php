<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

use Illuminate\Database\Eloquent\SoftDeletes;


class Sub_clients extends Model implements AuditableContract
{
  use Auditable;
  use \OwenIt\Auditing\Auditable;
//
  protected $table = 'users';


   use SoftDeletes;
   protected $dates = ['deleted_at'];
}
