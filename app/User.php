<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;

// use OwenIt\Auditing\Contracts\UserResolver;
use Auth;


class User extends Authenticatable implements AuditableContract
{
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    use HasApiTokens, Notifiable;
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','c_password','type', 'status', 'parent_id','contact', 'location', 'address','delete_flag','user_id','provider','provider_id',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','c_password', 'remember_token',
    ];

    // public static function resolveId(){
    //   return Auth::check() ? Auth::user()->getAuthIdentifier() :null;
    // }
}
